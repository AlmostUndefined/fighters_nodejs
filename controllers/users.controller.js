const UserService = require('../services/userService')
module.exports = { getUsers, getUser, createUser, updateUser, deleteUser }

async function getUsers (req, res, next) {
  try {
    res.json(await UserService.usersGet())
  } catch (e) {
    next(e)
  }
}

async function getUser (req, res, next) {
  try {
    res.json(await UserService.userGet(req.params.id))
  } catch (e) {
    next(e)
  }
}

async function createUser (req, res, next) {
  try {
    res.json(await UserService.userCreate(req.body))
  } catch (e) {
    next(e)
  }
}

async function updateUser (req, res, next) {
  try {
    res.json(await UserService.userUpdate(req.body, req.params.id))
  } catch (e) {
    next(e)
  }
}

async function deleteUser (req, res, next) {
  try {
    res.json(await UserService.userDelete(req.params.id))
  } catch (e) {
    next(e)
  }
}