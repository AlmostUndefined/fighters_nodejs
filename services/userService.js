const { UserRepository } = require('../repositories/userRepository');
const { badRequest, notFound } = require('../utility/errors');

class UserService {

    usersGet() {
        const users = UserRepository.getAll()
        if(!users) {
            return null;
        }
        return users;
    }

    userGet(id) {
        const user = UserRepository.getOne(id);
        if(!user) {
            return null;
        }
        return user;
    }

    userCreate(body) {
        const { email, password, firstName, lastName, phoneNumber, ...rest} = body;
        const data = { email, password, firstName, lastName, phoneNumber };
        const item = UserRepository.create(data);
        return item;
    }

    userUpdate(body, id) {
        const { email, password, firstName, lastName, phoneNumber, ...rest} = body;
        const data = { email, password, firstName, lastName, phoneNumber };
        const item = UserRepository.update(id, data);
        return item;
    }

    userDelete(id) {
        if (!userGet(id)) throw notFound(`This user doesn't exist`);
        const item = UserRepository.delete(id);
        return item;
    }
}

module.exports = new UserService();