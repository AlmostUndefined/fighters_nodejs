const AuthService = require('../services/authService');
module.exports = { authLogin }

async function authLogin (req, res, next) {
  try {
    res.json(await AuthService.login(req.body))
  } catch (e) {
    next(e)
  }
}