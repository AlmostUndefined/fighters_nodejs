const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    fightersGet() {
        const fighters = FighterRepository.getAll()
        if(!fighters) {
            return null;
        }
        return fighters;
    }

    fighterGet(id) {
        const fighter = FighterRepository.getOne(id);
        if(!fighter) {
            return null;
        }
        return fighter;
    }

    fighterCreate(body) {
        const { name, power, defense, ...rest} = body;
        const data = { name, power, defense };
        const fighter = FighterRepository.create(data);
        return fighter;
    }

    fighterUpdate(body, id) {
        const { name, power, defense, ...rest} = body;
        const data = { name, power, defense };
        const fighter = FighterRepository.update(id, data);
        return fighter;
    }

    fighterDelete(id) {
        if (!fighterGet(id)) throw notFound(`This fighter doesn't exist`);
        const fighter = FighterRepository.delete(id);
        return fighter;
    }
}

module.exports = new FighterService();