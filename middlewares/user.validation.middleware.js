const { user } = require('../models/user');
const { badRequest, notFound } = require('../utility/errors');
const userService = require ('../services/userService');

const createUserValid = async (req, res, next) => {
  try {
    if (req.body.id) throw badRequest('You have no permission to add Id');
    if (!(/[@]gmail.com$/).test(req.body.email)) throw badRequest('Not valid email');
    if (!(/^[+]380\d{9}$/).test(req.body.phoneNumber)) throw badRequest('Not valid phone number');
    const users = userService.usersGet();
    for (let i = 0; i < users.length; i++) {
      if (users[i].email == req.body.email) throw badRequest('This email already registered');
      if (users[i].phoneNumber == req.body.phoneNumber) throw badRequest('This phone number already registered');
    }
    if (!(/\w{1}/).test(req.body.firstName)) throw badRequest('Not valid first name');
    if (!(/\w{1}/).test(req.body.lastName)) throw badRequest('Not valid last name');
    if (!(/(\w|\W){3}/).test(req.body.password)) throw badRequest('Not valid password');
    next()
  } catch (e) {
    next(e)
  }
}

const updateUserValid = (req, res, next) => {
  try {
    if (!(/[@]gmail.com$/).test(req.body.email)) throw badRequest('Not valid email');
    if (!(/^[+]380\d{9}$/).test(req.body.phoneNumber)) throw badRequest('Not valid phone number');
    if (!(/\w{1}/).test(req.body.firstName)) throw badRequest('Not valid first name');
    if (!(/\w{1}/).test(req.body.lastName)) throw badRequest('Not valid last name');
    if (!(/(\w|\W){3}/).test(req.body.password)) throw badRequest('Not valid password');
    next()
  } catch (e) {
    next(e)
  }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;