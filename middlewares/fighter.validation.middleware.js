const { fighter } = require('../models/fighter');
const { badRequest, notFound } = require('../utility/errors');
const fighterService = require ('../services/fighterService');

const createFighterValid = (req, res, next) => {
  try {
    if (req.body.id) throw badRequest('You have no permission to add Id');
    if (!(/\w{1}/).test(req.body.name)) throw badRequest('Not valid fighter name');
    if (!(100 > parseInt(req.body.power) && 0 < parseInt(req.body.power))) throw badRequest('Not valid power field');
    if (!(10 >= parseInt(req.body.defense) && 0 < parseInt(req.body.defense))) throw badRequest('Not valid defense field');
    const fighters = fighterService.fightersGet();
    for (let i = 0; i < fighters.length; i++) {
      if (fighters[i].name == req.body.name) throw badRequest('Fighters with this name is already exist');
    }
    next()
  } catch (e) {
    next(e)
  }
}

const updateFighterValid = (req, res, next) => {
  try {
    if (!(100 > parseInt(req.body.power) && 0 < parseInt(req.body.power))) throw badRequest('Not valid power field');
    if (!(10 >= parseInt(req.body.defense) && 0 < parseInt(req.body.defense))) throw badRequest('Not valid defense field');
    if (!(/\w{1}/).test(req.body.name)) throw badRequest('Not valid fighter name');
    next()
  } catch (e) {
    next(e)
  }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;