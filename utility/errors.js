class LocalError extends Error {
  badRequest (message) {
    return {
      status: 400,
      message
    }
  }

  notFound (message) {
    return {
      status: 404,
      message
    }
  }
}
const localError = new LocalError()
exports.badRequest = localError.badRequest
exports.notFound = localError.notFound