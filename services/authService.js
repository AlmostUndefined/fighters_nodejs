const UserService = require('./userService');
const { notFound } = require('../utility/errors');

class AuthService {
  login(userData) {
    const user = UserService.userGet(userData);
    if(!user) {
      throw notFound('User not found');
    }
    return user;
  }
}

module.exports = new AuthService();