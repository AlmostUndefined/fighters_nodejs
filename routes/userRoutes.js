const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const usersController = require('../controllers/users.controller');

const router = Router();

router.get('/', usersController.getUsers)
router.get('/:id', usersController.getUser)
router.post('/', createUserValid, usersController.createUser)
router.put('/:id', updateUserValid, usersController.updateUser)
router.delete('/:id', usersController.deleteUser)

module.exports = router;