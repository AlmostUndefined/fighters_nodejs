const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const authController = require('../controllers/auth.controller');

const router = Router();

router.post('/login', authController.authLogin)

module.exports = router;