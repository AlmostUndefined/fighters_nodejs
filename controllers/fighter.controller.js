const FighterService = require('../services/fighterService');
module.exports = { getFighters, getFighter, createFighter, updateFighter, deleteFighter }

async function getFighters (req, res, next) {
  try {
    res.json(await FighterService.fightersGet())
  } catch (e) {
    next(e)
  }
}

async function getFighter (req, res, next) {
  try {
    res.json(await FighterService.fighterGet(req.params.id))
  } catch (e) {
    next(e)
  }
}

async function createFighter (req, res, next) {
  try {
    res.json(await FighterService.fighterCreate(req.body))
  } catch (e) {
    next(e)
  }
}

async function updateFighter (req, res, next) {
  try {
    res.json(await FighterService.fighterUpdate(req.body, req.params.id))
  } catch (e) {
    next(e)
  }
}

async function deleteFighter (req, res, next) {
  try {
    res.json(await FighterService.fighterDelete(req.params.id))
  } catch (e) {
    next(e)
  }
}