const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');
const fighterController = require('../controllers/fighter.controller');

const router = Router();

router.get('/', fighterController.getFighters)
router.get('/:id', fighterController.getFighter)
router.post('/', createFighterValid, fighterController.createFighter)
router.put('/:id', updateFighterValid, fighterController.updateFighter)
router.delete('/:id', fighterController.deleteFighter)

module.exports = router;